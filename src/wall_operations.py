import numpy as np
import math as m
'''

'''
# wall parameter - start_x,start_y,end_x,end_y
#walls=np.array([[0,0,10,0],[0,5,10,5]])

#Temp postions x,y
#pos_x=np.floor(5*np.random.rand(5,1))
#pos_y=np.floor(5*np.random.rand(5,1))
#print(pos_x)
#print(pos_y)
#velocity=np.array([])
ri = 0.23

def corrct_e_dir(dist_x,dist_y,a,b):
    ''' Direction correction for the E()'''
    e_x,e_y=e_fun(abs(dist_x),abs(dist_y),a,b)
    if dist_x > 0:
        e_x *=-1
    if dist_y > 0:
        e_y *=-1
    return e_x,e_y


def e_fun(dx,dy,a,b):
    '''Calculation of the repulsive force given distance'''
    #check with alex about the minus sign

    #To-Do comment - radius
    
    if dx == 0:
        #e_y = a*np.exp(b*(dy))
        e_y = a*(np.exp((ri-dy)/b))
        e_x = 0.
    if dy == 0:
        #e_x = a*np.exp(b*(dx))
        e_x = a*(np.exp((ri-dx)/b))
        e_y = 0.

    return e_x,e_y

def sum_e_force(walls,pos_x,pos_y,index):
    '''Calculate the sum of the repulsive force'''
    sum_e_x = 0.0
    sum_e_y = 0.0

    for wall in walls:
        #print(wall)
        dist_x,dist_y=calc_distance_to_wall(wall,pos_x[index],pos_y[index])
        #print('dx:{},dy:{}'.format(dist_x,dist_y))
        #test_vals
        
        a = 2000.0
        b = 0.08
        ri = 0.23

        e_x,e_y = corrct_e_dir(dist_x,dist_y,a,b)
        #print('ex:{},ey:{}'.format(e_x,e_y))
        sum_e_x +=e_x
        sum_e_y +=e_y

    return (sum_e_x,sum_e_y)

def e_vec(walls,pos_x,pos_y):
    ''' calculate for all particles inthe system and return two component force vectors '''
    exs = np.zeros_like(pos_x)
    eys = np.zeros_like(pos_x)
    for indx in range(0,len(pos_x)):
        ex,ey= sum_e_force(walls,pos_x,pos_y,indx)
        exs[indx] = ex
        eys[indx] = ey
        #print('f-{},{}'.format(ex,ey))
    return exs,eys

def calc_distance_to_wall(wall,x,y,ped_index=[]):
    '''Given wall and particle calculate distance  '''
    if wall[0]==wall[2]:
        #print("vertical wall")
        dx = wall[0]-x
        dy = 0.0

    if wall[1]==wall[3]:
        #print("horizontal wall")
        dy = wall[1]-y
        dx = 0.0
    return dx,dy

