# -*- coding: utf-8 -*-
"""
Created on Thu May  9 14:48:36 2019

@author: Olufade ONIFADE
"""

import argparse
import simulation
import visualization


def main():
    '''
    Run a simulation of pedestrian traffic 
    '''
    parser = argparse.ArgumentParser(description='Run simulation of pedestrians')
    parser.add_argument( '-n', metavar='N', type=int, nargs='?', dest='num',
                        help='The number of pedestrians', default=10)
    parser.add_argument( '-t', metavar='I', type=int, nargs='?', dest='iters',
                        help='Number of iterations', default=1000)
    #parser.add_argument('-f', metavar='fname',type = str, help = 'output filename',
     #                   default='')
    #parser.add_argument('-fn', dest='accumulate', action='store_const',
     #                   const=sum, default=10, type=string
      #                  help='Pass the number of pedestrians')
    
    print(
        '''
        ####################################
        Pedestrian Dynamics Simulator - B2 Walkers
        ####################################
        
        Version 1.0
        Straight corridor layout
        corridor length = 10 pixels
        corridor width = 5 pixels
        
        Default number of pedestrians: 10
        number of iterations: 1000
          
        '''      
    )
    
    
    args = parser.parse_args()
    num_peds = args.num
    num_iters = args.iters
    #print(num_peds, time_iters)
    
    fname = '../output_{}pedestrians.csv'.format( num_peds )
    print('''
          Running simulation for {} pedestrians for {} iterations.
          '''.format( num_peds, num_iters ) )
    
    simulation.main(num_peds, num_iters)
    
    print('''
          Simulation output has been saved in {}.
          Now running animation...
        '''.format( fname ) )
    
    visualization.main(num_peds, fname)
    
    

if __name__ == '__main__':
    main()