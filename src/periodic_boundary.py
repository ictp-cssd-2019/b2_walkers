def pbc(pos_x,length,width,number_of_pedestrians):
	'''
	this function updates the position of pedestrians that surpass the limits of the corridor
	in the x-direction
	'''
	i = 0
	padding = 5.0
	while i<number_of_pedestrians:
		if(pos_x[i]<-padding):
			pos_x[i]=length+padding
		if(pos_x[i]>length+padding):
			pos_x[i]=0.0-padding
		i+=1
	return pos_x
