from nose.tools import with_setup, raises
from wall_operations import *

def test_1():
    "Test distance calculation"
    walls=[[0,0,2,0]]
    pos_x=np.array(1.0)
    pos_y=np.array(1.0)
    dx,dy =  calc_distance_to_wall(walls[0],pos_x,pos_y,0) 
    #print(dx,dy)
    assert (dx == 0 and dy == -1)

def test_2():
    "Test repultion force of distance wall"
    walls = [[0,0,10,0]]
    pos_x = np.array(1.0)
    pos_y = np.array(10.0)
    dx,dy = calc_distance_to_wall(walls[0],pos_x,pos_y,0)
    ex,ey = e_fun(dx,dy,1,1)
    print(ex,ey)
    
