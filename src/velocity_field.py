'''
VELOCITY FIELD MODULE (F)
-------------------------
This module calls a function that calculates the velocity field with the given equation:

F(xi,xj) = (vd(xi) - xdi)/tau

where vd is an initial value from the velocity field of interest for each position x,
xdi is the current velocity of the pedestrian, and tau is an initial value for
the relaxation time

'''

import numpy as np

tau = 0.05

def velocity_field_function (vdx,vdy,vel_x,vel_y,category):
	'''
	VELOCITY FIELD FUNCTION
	--- This function expects an initial float value for the x velocity, y velocity,
	    x velocity field and y velocity field stored in vel_x, vel_y, vdx, vdy; as 
	    well as an integer value containing (-1,1) for the category to determine 
	    its velocity direction in category. It returns a float velocity field force 
	    for both the x and y component, namely Fx and Fy.
	'''
	Fx = (vdx * category - vel_x)/tau
	Fy = (vdy * category - vel_y)/tau
	return Fx, Fy
