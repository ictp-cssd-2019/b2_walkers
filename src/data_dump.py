import numpy as np

def add_comment(comment,file_name):
	dumper_c = open(file_name,"w")
	dumper_c.write('#' + comment + "\n")
	dumper_c.close()

def dump_arrays(file_name, number_of_pedestrian, time_step, index, pos_x, pos_y, vel_x, vel_y, category):
	dumper_a = open(file_name,"a")
	ctr = 0
	while ctr < number_of_pedestrian:
		dumper_a.write(str(time_step) + ',' + str(index[ctr]) + ',' + str(pos_x[ctr]) + ',' + str(pos_y[ctr]) + ',' + str(vel_x[ctr]) + ',' + str(vel_y[ctr]) + ',' + str(category[ctr]) + "\n")
		ctr += 1
	dumper_a.close()

def clear_all(file_name):
	open(file_name, "w").close()
