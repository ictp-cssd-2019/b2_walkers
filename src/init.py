'''
This script creates the initial conditions of pedestrians
'''

import numpy as np

def create_initial_conditions(number_of_pedestrians,length,width):
	'''
	Creates initial conditions for pedestrians in a straight corridor
	'''
	i=0
	space = 0.3 	# the space between the pedestrians and the wall position

	pos_x = np.random.uniform(low=space, high=length, size=number_of_pedestrians)
	pos_y = np.random.uniform(low=space, high=width-space, size=number_of_pedestrians)
	vel_x = np.zeros(number_of_pedestrians)
	vel_y = np.zeros(number_of_pedestrians)
	category = np.random.binomial(1, 0.5, number_of_pedestrians) # Is the type of pedestrian, defines the direction of the desired velocity
	category[category == 0] = -1
	
	return pos_x,pos_y,vel_x,vel_y,category

