from velocity_field import velocity_field_function as desiredF
import social_force_module as social  
import wall_operations as wall_force

def leapfrog(pos_x,pos_y,vel_x,vel_y,F_x, F_y,category, dt,vdx,vdy,number_of_pedestrians,walls):
	""" 
	This is the integration script to get the new position and velocities from the initial posotion and velocities based on leapfrog algorithm. 
	"""
        

     
	vel_x = vel_x + 0.5 * F_x * dt
	vel_y = vel_y + 0.5 * F_y * dt
    
	pos_x = pos_x + vel_x * dt
	pos_y = pos_y + vel_y * dt

	F_x,F_y = sum_of_forces(pos_x,pos_y,vel_x,vel_y,F_x, F_y,category, dt,vdx,vdy,number_of_pedestrians,walls)
	vel_x = vel_x + 0.5 * F_x * dt
	vel_y = vel_y + 0.5 * F_y * dt    

	return pos_x, pos_y, vel_x, vel_y, F_x, F_y


def sum_of_forces(pos_x,pos_y,vel_x,vel_y,F_x, F_y,category, dt,vdx,vdy,number_of_pedestrians,walls):

	F_x1, F_y1 = desiredF(vdx,vdy,vel_x,vel_y,category) 	 

	F_x2, F_y2 = social.social_functionK(number_of_pedestrians,pos_x,pos_y,category) 
	F_x3, F_y3 = wall_force.e_vec(walls,pos_x,pos_y)

	F_x_total = F_x1 + F_x2 + F_x3
	F_y_total = F_y1 + F_y2 + F_y3

	return F_x_total,F_y_total
