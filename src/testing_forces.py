import unittest
import velocity_field 

class TestForces(unittest.TestCase):
    def test_direction_desired_force(self):
        """
        Test that pedestrians with category -1 have negative force
        and pedestrians with category +1 have positive force 
        """
        category = 1  # Should produce positive force
        vdx = 2
        vdy = 0
        vel_x = 1
        vel_y = 0

        Fx, Fy = velocity_field.velocity_field_function(vdx,vdy,vel_x,vel_y,category)
        self.assertTrue(Fx>0.0)

        category = -1  # Should produce positive force
        vdx = 2
        vdy = 0
        vel_x = 1
        vel_y = 0

        Fx, Fy = velocity_field.velocity_field_function(vdx,vdy,vel_x,vel_y,category)
        self.assertTrue(Fx<0.0)


if __name__ == '__main__':
    unittest.main()


'''


class TestSum(unittest.TestCase):
    def test_list_int(self):
        """
        Test that it can sum a list of integers
        """
        data = [1, 2, 3]
        result = sum(data)
        self.assertEqual(result, 6)

    def test_list_fraction(self):
        """
        Test that it can sum a list of fractions
        """
        data = [Fraction(1, 4), Fraction(1, 4), Fraction(2, 5)]
        result = sum(data)
        self.assertEqual(result, 1)

if __name__ == '__main__':
    unittest.main()
'''