'''
##########################################
B2_walkers - Pedestrian Dynamics Simulator
##########################################

This script solves the trajectories of N pedestrians walking along a straight corridor.
The dimensions of the corridors are parameters provided by the user. 
The pedestrians follow the Social Force Model, the parameters of the forces are the fixed
according to the original values. The desired velocity is vd = (1,0) fixed parameter

The integration is done with Velocity verlet. The program consist of 3 basic steps:
- Set up the initial configuration
- Loop over time
- Sampling loop: Update the positions and velocities until the sampling frequency. Once 
it is done, the results of the configurations are return in a csv file
'''

import numpy as np
import random
import leapfrog_int as integrate
import init 
import data_dump as dump
import matplotlib.pyplot as plt
import periodic_boundary as pb


def main(number_of_pedestrians, n_time_max):


	#number_of_pedestrians = 100
	index = np.linspace(1,number_of_pedestrians,number_of_pedestrians)
	#dt = 0.0001
	length = 10.0
	width = 5.0
	ax = np.zeros(number_of_pedestrians)
	ay = np.zeros(number_of_pedestrians)
	vdx = 2.0
	vdy = 0.0
	walls=np.array([[0,0,length,0],[0,width,length,width]])

	results_initial_condition = init.create_initial_conditions(number_of_pedestrians,length,width)
	pos_x = results_initial_condition[0]
	pos_y = results_initial_condition[1]
	vel_x = results_initial_condition[2]
	vel_y = results_initial_condition[3]
	category = results_initial_condition[4]

	######################################
	# contstants
	#n_time_max = 100000
	n_sample_max = 50 
	dt = 0.001

	######################################	

	time_sample = dt*n_sample_max

	n_time = 0
	time = 0
	#tmp_val=[]
	dump.clear_all('output_{}pedestrians.csv'.format(number_of_pedestrians))
	dump.add_comment('TIMESTEP, INDEX, POS_X, POS_Y, VEL_X, VEL_Y, CATEGORY','output_{}pedestrians.csv'.format(number_of_pedestrians))
	while n_time<n_time_max:
		'''
		Loop over time (from the bigining to the end)
		'''

		n_sample = 0
		while n_sample<n_sample_max:
			'''
			Sampling loop, in this loop we perform the integration. Once the loop is over, we dump the state of the system
			'''

			pos_x,pos_y,vel_x,vel_y, ax, ay = integrate.leapfrog(pos_x,pos_y,vel_x,vel_y,ax,ay,category,dt,vdx,vdy,number_of_pedestrians,walls)

			n_sample+=1
		pos_x = pb.pbc(pos_x,length,width,number_of_pedestrians)  # adds periodic boundary conditions to x-direction
		n_time+=n_sample_max
		#print("Iterations so far: {}".format( n_time ) )
		time += time_sample
		dump.dump_arrays('../output_{}pedestrians.csv'.format(number_of_pedestrians), number_of_pedestrians, time, index, pos_x, pos_y, vel_x, vel_y, category)

##Please make sure you keep this at all time for documentation
#if __name__ == '__main__':
#    main()
