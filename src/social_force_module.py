'''
SOCIAL FORCE MODULE (K)
------------------------
This module calls a function that calculates the social force with the given equation:

summation(i!=j and j=1 to total pedestrians): K(xi,xj) = A exp((-abs(xi-xj)**2)/R**2)*((xi-xj)/abs(xi-xj))*theta(xi-xj,vd)

where A and R are constants with A = 2000.N and R = 0.08m,
theta is assumed as 1,
xi and xj are numpy arrays of positions,
and K is a numpy array of forces

'''

# call this function every time step
# A = 2000 (constant for force (N))
# R = constant?
# theta = fixed angle?

import numpy as np
import math as m

def social_functionK (number_of_pedestrian,pos_x,pos_y,category):
	'''
	SOCIAl FORCE FUNCTION
	--- This function expects a global variable of the total number of pedestrians
	    and numpy arrays of initial position for x and y coordinates stored in pos_x 
	    and pos_y. It returns numpy arrays of calculated social force for x and y 
	    component stored in soc_force_Kx and soc_force_Ky.
	'''
	soc_force_Kx = np.zeros(number_of_pedestrian)
	soc_force_Ky = np.zeros(number_of_pedestrian)
	A = 2000.
	B = 0.08
	rij = 0.46

	index_id = 0

	while index_id < number_of_pedestrian:
		idxy = 0
		scx = 0
		scy = 0
		Kx = 0.
		Ky = 0.
		scal_x = np.zeros(number_of_pedestrian)
		scal_y = np.zeros(number_of_pedestrian)

		while scx < number_of_pedestrian:
			if index_id == scx:
				scx += 1
				pass
			else:
				scal_x[scx] = pos_x[index_id] - pos_x[scx]
				scx += 1

		while scy < number_of_pedestrian:
			if index_id == scy:
				scy += 1
				pass
			else:
				scal_y[scy] = pos_y[index_id] - pos_y[scy]
				scy += 1

		while idxy < number_of_pedestrian:
			if index_id == idxy:
				idxy += 1
				pass
			else:
				scal_xy = m.sqrt((scal_x[idxy]**2) + (scal_y[idxy]**2))
				#expo = ((scal_xy)**2)/(B**2)
				expo = -((rij-scal_xy))/(B)
				sub_x = scal_x[idxy]/scal_xy
				sub_y = scal_y[idxy]/scal_xy
				theta = angle_theta(category[index_id],scal_x[idxy],scal_y[idxy])
				Kx = Kx + A * m.exp(-expo) * sub_x * theta
				Ky = Ky + A * m.exp(-expo) * sub_y * theta
				idxy += 1

		soc_force_Kx[index_id] = Kx
		soc_force_Ky[index_id] = Ky


		index_id += 1

	return soc_force_Kx, soc_force_Ky


def angle_theta(category,scal_x,scal_y):
	vector1 = np.array([scal_x,scal_y])
	vector2 = np.array([category,0.])

	dot_prod = np.dot(vector1,vector2)

	scal_v1 = m.sqrt((vector1[0]**2)+(vector1[1]**2))
	scal_v2 = m.sqrt((vector2[0]**2)+(vector2[1]**2))
	scal_sum = scal_v1 + scal_v2


	theta = m.acos(dot_prod/scal_sum)


	if theta <= 80:
		mult = 1
	elif theta > 80:
		mult = 0

	return mult
