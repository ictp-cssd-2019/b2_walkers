import matplotlib
matplotlib.use("Agg")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pandas as pd

def loadData(file_nm):
    '''
    Load pedestrian data from file
    returns a tuple of numpy arrays: ...
    '''
           
    #Load pedestrian data from file
    ped_data = pd.read_csv(
        file_nm,
        sep=',',
        comment='#',
        index_col=False,
        names = [ 't_step', 'id', 'x', 'y', 'vx', 'vy', 'type' ],
        dtype = {
            't_step': np.float64,
            'id': int,
            'x': np.float64,
            'y': np.float64,
            'vx': np.float64,
            'vy': np.float64,
            'type': int
            }
    )
    
    #Pedestrian positions.
    # arrays are arranged row_wise
    ped_pos = np.array( [ ped_data['x'].values,
                        ped_data['y'].values ] )
    
    #Pedestrian velocities
    ped_vels = np.array( [ ped_data['vx'].values,
                        ped_data['vx'].values ] )
    
    #Pedestrian IDs and types (direction of motion)
    ped_type = np.array(ped_data['type'])
    
    return ( np.transpose(ped_type), np.transpose(ped_pos), np.transpose( ped_vels) )


def readInitFile():
    '''
    Read data to initialize simulation
    The __init__.config contains the following information in the exact order
    
    file name: <value>
    pedestrian population: <value>
    no of timesteps: <value>
    simulation interval: <value>
    walls (max width): <value>
    walls (max height): <value>
    output video file name: <value>
    
    Be sure to specify the values in the above order.
    
    Note that the filename doesn't need surrounding quotes ("")
    and remember to include the file extension with all filenames
    
    '''
    value = []
    walls = []
    with open('../__init__.config') as file:
        
        for line in file:
            _, data = line.split(':')
            value.append( data.strip() )
            
    try:
        #file_name = value[0]
        #ped_pop = int( value[1])
        #num_ts = int( value[2] )
        sim_intv = int( value[0] )
        walls.append( int( value[1] ) )
        walls.append( int( value[2] ) )
        out_file = value[3]
        #fps = int( value[4] )
        
    except:
        print('''
              Bad Input read from __init__.config file.
              Check the parameters and ensure they are in the correct order
              ''')
    
    return ( sim_intv, walls, out_file )




def splitData(in_data, type_data, n, frm_num):
    '''
    Split pedestrian data into different timesteps
    ind_data: array of pedestrian positions
    type_data: pedestrian type based on direction of motion
    n: number of pedestrians at each timestep
    frm_num: frame number
        
    '''
    start = n * frm_num
    end = start + n
    xs = in_data[start:end, 0]
    ys = in_data[start:end, 1]
    cats = type_data[start:end]
    return xs, ys, cats

def main(ped_pop, file_name):
    '''
    The main function that shows the simulation
    
    To save the visualization as a video frame,
    
    '''    

    ( sim_intv, walls, out_file ) = readInitFile()
    
    
    ped_cat, peds_pos, peds_vel = loadData(file_name)
    #print(peds_pos.shape)
    
    num_ts = peds_pos.shape[0] // ped_pop
    
    fig, ax = plt.subplots()
    
    max_x = walls[0] #np.ceil( np.max(peds_pos[:,0]) )
    max_y = walls[1] #np.ceil( np.max(peds_pos[:,1]) )
    
    #min_x = np.floor( np.min(peds_pos[:,0]) )
    #min_y = np.floor( np.min(peds_pos[:,1]) )
    
    ax.set_xlim(0, max_x)
    ax.set_ylim(0, max_y)
    
    line, = ax.plot([], [], 'bo', ms=10)
    line1,= ax.plot([], [], 'ro', ms=10)
    
    ax.plot([0, max_x], [0, 0], 'k') # Lower wall
    ax.plot([0, max_x], [max_y, max_y], 'k') # Upper wall
    #print("Here...Before animate()")
    
    def animate(frame_number):
        '''
        Update the data in the plot at every frame
        '''
        #print("Here...After animate()")
        xs, ys, typ = splitData( peds_pos, ped_cat, ped_pop, frame_number )
        
        #if frame_number >= num_ts:
            #exit(0)
        
        print( 'Frame number: {}'.format(frame_number))
         
        y_1 = ys[typ == -1]
        x_1 = xs[typ == -1]
        
        x_2 = xs[typ == 1]
        y_2 = ys[typ == 1]
        #print(x_1)
        #exit(0)
        line.set_data(x_1, y_1)  # update the data
        line1.set_data(x_2, y_2)
        plt.show()
        plt.savefig( out_file + '_{0:05d}.png'.format( frame_number )  )
        
        return (line, line1)
    
    ax.set_axis_off()
    
    for frm_n in range(num_ts):
        animate(frm_n)
    
#    ani = animation.FuncAnimation(fig, animate, 
#                                  interval=sim_intv, 
#                                  blit=True)

    #if len(out_file) > 0:
     #   saveAnim(ani, out_file, fps)
    # or
    #plt.show()


    # other walls?

    # gravity ?

    # collision()

    # energy conservation?
def saveAnim(ani, fname, fp_sec):
    pass
    #ani.save(fname, fps=fp_sec, extra_args=['-vcodec', 'libx264'] )
    #ani.save('../2_pedestrians.mp4', fps=30, extra_args=['-vcodec', 'libx264'] )
    
    
    
#if __name__ == '__main__':
#    main()

# Based on 
#
# Animation of Elastic collisions with Gravity
# 
# author: Jake Vanderplas
# email: vanderplas@astro.washington.edu
# website: http://jakevdp.github.com
# license: BSD
# Please feel free to use and modify this, but keep the above information. Thanks!


