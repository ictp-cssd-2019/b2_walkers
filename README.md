This B2_walkers a Pedestrian Dynamics Simulator.


This software is distributed under the GNU General Public License.

----------------------------------------------------------------------

B2_walkers is a classical pedestrian dynamics simulation code.
It was developed by Hasith Perera, Don Niko R Godilano,
Joseph AKINYEM,  Sita Sundar and Ignacio Sticco at the ICTP Trieste 
in the context of 7th Workshop on Collaborative Scientific Software Development 

The B2_walkers distribution includes the following files and directories:

b2_walker_main.py  
init.py
social_force_module.py
testing_forces.py
visualization_2.py
data_dump.py
main_test.py
periodic_boundary.py
velocity_field.py
visualization.py
leapfrog_int.py
simulation.py
velocity_field.pyc
wall_operations.py



Point your browser at any of these files to get started:


https://ictp-cssd-2019.gitlab.io/b2_walkers/
https://gitlab.com/ictp-cssd-2019/b2_walkers



