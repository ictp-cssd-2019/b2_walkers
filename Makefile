PY_SOURCES = \
src/data_dump.py \
src/init.py \
src/leapfrog_int.py\
src/main_test.py\
src/periodic_boundary.py\
src/simulation.py\
src/social_force_module.py\
src/testing_forces.py\
src/velocity_field.py\
src/visualization.py\
src/visualization_2.py\
src/wall_operations.py


TARGETS = \
300pedestrians


all : $(patsubst %,video_%_pbc.gif, $(TARGETS))


video_%_pbc.gif : output_%.csv src/visualization.py
	python3 src/visualization.py $<

output_%.csv : $(PY_SOURCES)
	python3 src/simulation.py $*

