FROM python:3.7

RUN pip install numpy matplotlib pandas
RUN apt-get update -y
RUN apt-get install ffmpeg git -y


COPY ./execute_sim.sh .
RUN chmod +x execute_sim.sh;
