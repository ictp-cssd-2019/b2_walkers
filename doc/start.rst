
Getting started
===============

The project contains all required python files to run a pedestrian dynamic simulation.To get started

Option 1
++++++++

You need to install numpy and matplotlib library to install the simulation module:
    .. code-block:: bash

        sudo apt-get install python3-tk
        pip install numpy matplotlib pandas
        git clone https://gitlab.com/ictp-cssd-2019/b2_walkers.git
        
Option 2
++++++++

You can use the pre-build docker configuration file to configure a python 3.7 image to do the execution

