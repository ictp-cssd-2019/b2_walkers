Simulation details
==================



.. automodule:: simulation
.. autofunction:: simulation.init

This is the initial configuration file wherein we provide the input position and velocity of all the pedestrians and wall forces.



   simulation
   ++++++++++
   
.. automodule:: simulation
   :members:
   
   integration
   +++++++++++
   
.. automodule:: leapfrog_int
   :members:
   
   Forces
   ++++++
   
   wall repultion force
   --------------------
   
.. automodule:: wall_operations
   :members:
   
.. toctree::
   :maxdepth: 2
   :caption: Contents:

