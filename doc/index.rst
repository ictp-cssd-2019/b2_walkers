.. b2_walker documentation master file, created by
   sphinx-quickstart on Wed May  8 14:16:10 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pedestrian dynamics simulation documentation!
========================================================

.. image:: ./imgs/output.gif
   :height: 200px
   :align: center

Pedestrian dynamics is an important physical problem with real-life implications. To improve the pedestrian behaviour, we tried to simulate the same using python scrips. Herein, we provide the details of the simulation algorithm, tutorial to reproduce the dynamics supported with examples, and the visualization tools for the same.

Our project b2_walker basically facilitates the simulation and visualization of pedestrian dynamics. Simulation consists of python scripts to perform the simulations.
('https://github.com/sphinx-doc/sphinx/issues/%s')



All the basic componets of the dynamics can be reproduced by simulating the exact forces acting between the pedestrians as well as their statistical effect. 


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   start
   slides
   simulation


Indices and tables


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`




